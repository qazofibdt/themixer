﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

public static class IconUtils
{
    public static Icon ExtractAssociatedJumboIcon(string path)
    {
        SHFILEINFO fileInfo = new SHFILEINFO();
        Shell32.SHGetFileInfo(path, 0, ref fileInfo,
            (uint)System.Runtime.InteropServices.Marshal.SizeOf(fileInfo),
            (uint)(SHGFI.SysIconIndex | SHGFI.LargeIcon | SHGFI.UseFileAttributes));

        var iIcon = fileInfo.iIcon;

        IImageList imageList = null;
        Guid guil = new Guid(Shell32.IID_IImageList2); //or IID_IImageList

        Shell32.SHGetImageList(Shell32.SHIL_JUMBO, ref guil, ref imageList);
        IntPtr hIcon = IntPtr.Zero;
        imageList.GetIcon(iIcon, Shell32.ILD_TRANSPARENT | Shell32.ILD_IMAGE, ref hIcon);

        Icon icon = (Icon)Icon.FromHandle(hIcon).Clone();

        User32.DestroyIcon(hIcon);

        return icon;
    }

    public static void ScaleIconBitmap(ref Bitmap bmp, int size)
    {
        double scale = (double)size / bmp.Width;

        var scaledBmp = new Bitmap(size, size);
        var graph = Graphics.FromImage(scaledBmp);

        graph.InterpolationMode = InterpolationMode.High;
        graph.CompositingQuality = CompositingQuality.HighQuality;
        graph.SmoothingMode = SmoothingMode.AntiAlias;

        var brush = new SolidBrush(Color.Black);
        graph.FillRectangle(brush, -1, -1, size + 1, size + 1);
        graph.DrawImage(bmp, new Rectangle(0, 0, size, size));

        bmp = scaledBmp;
    }
}
