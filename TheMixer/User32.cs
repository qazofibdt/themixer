﻿using System;
using System.Runtime.InteropServices;

public static class User32
{
    [DllImport("user32.dll", EntryPoint = "DestroyIcon", SetLastError = true)]
    public static unsafe extern int DestroyIcon(IntPtr hIcon);

    [DllImport("user32.dll")]
    public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out UInt32 ProcessId);

    [DllImport("user32.dll")]
    public static extern IntPtr GetForegroundWindow();
}
