using System;
using System.Collections.Concurrent;
using System.ComponentModel;

using CSCore.CoreAudioAPI;
using System.Diagnostics;

using SessionId = System.String;

// Base class for all session event args
class SessionEventArgs : EventArgs
{
    public SessionEventArgs(SessionId sessionId)
    {
        SessionId = sessionId;
    }

    public SessionId SessionId { get; }
}

class SessionAddedEventArgs : SessionEventArgs
{
    // TODO should this event take the actual session object, or a snapshot of its properties?
    public SessionAddedEventArgs(SessionId id, AudioSessionControl2 session) : base(id)
    {
        Session = session;
    }
    
    public AudioSessionControl2 Session { get; }
}

class SessionRemovedEventArgs : SessionEventArgs
{
    public SessionRemovedEventArgs(SessionId id) : base(id)
    {
    }
}

class VolumeChangedEventArgs : SessionEventArgs
{
    public VolumeChangedEventArgs(SessionId sessionId, float volume, bool isMuted)
        : base(sessionId)
    {
        Volume = volume;
        IsMuted = isMuted;
    }
    
    public float Volume { get; }
    public bool IsMuted { get; }
}

class StateChangedEventArgs : SessionEventArgs
{
    public StateChangedEventArgs(SessionId sessionId, AudioSessionState state) : base(sessionId)
    {
        State = state;
    }

    public AudioSessionState State { get; }
}

class DisconnectedEventArgs : SessionEventArgs
{
    public DisconnectedEventArgs(SessionId sessionId, AudioSessionDisconnectReason reason) : base(sessionId)
    {
        Reason = reason;
    }

    public AudioSessionDisconnectReason Reason { get; }
}

interface IAudioSessions : IDisposable
{
    event EventHandler<SessionAddedEventArgs> SessionAdded;
    event EventHandler<SessionRemovedEventArgs> SessionRemoved;
    event EventHandler<VolumeChangedEventArgs> VolumeChanged;
    event EventHandler<StateChangedEventArgs> StateChanged;

    void Activate();

    // The following methods take a SessionId. Note that the identified session might be released
    // asynchronously before you call these methods, in which case the given ID will become
    // invalid and they will return false.

    // Sets the volume of the session with the given ID to the given value. Returns false if the
    // given ID is not valid.
    bool SetVolume(SessionId id, float volume);
    // Sets the mute state of the session with the given ID to the given value. Returns false if
    // the given ID is not valid.
    bool SetMute(SessionId id, bool mute);
}

/// <summary>
/// Keeps track of audio sessions in a sane way. Clients should construct an instance, register
/// event handlers, and then call Activate to begin tracking audio sessions
/// </summary>
class AudioSessions : IAudioSessions
{
    public event EventHandler<SessionAddedEventArgs>    SessionAdded;
    public event EventHandler<SessionRemovedEventArgs>  SessionRemoved;
    public event EventHandler<VolumeChangedEventArgs>   VolumeChanged;
    public event EventHandler<StateChangedEventArgs>    StateChanged;
    public event EventHandler<DisconnectedEventArgs>    Disconnected;

    // TODO remove, testing
    public void PrintSessions()
    {
        Console.Write("\n\n");

        Console.WriteLine("---Sessions---");
        foreach (var kvp in _sessions)
        {
            var sessionId = kvp.Key;
            var session = kvp.Value;

            Console.WriteLine();

            string processName;
            string windowName;
            try
            {
                processName = session.Process.ProcessName;
                if (session.IsSystemSoundSession)
                    processName += " (system)";
                if (session.IsSingleProcessSession)
                    processName += " (multi-process)";

                windowName = session.Process.MainWindowTitle;
            }
            catch
            {
                processName = "(Error)";
                windowName = "(Error)";
            }

            Console.WriteLine("Process name: " + processName);
            Console.WriteLine("Main window name: " + windowName);
            Console.WriteLine(sessionId);
            Console.WriteLine();
        }

        Console.Write("\n\n");
    }

    public AudioSessions()
    {
        _device = MMDeviceEnumerator.DefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
        _sessionManager = AudioSessionManager2.FromMMDevice(_device);

        _sessions = new ConcurrentDictionary<SessionId, AudioSessionControl2>();
    }

    // Activates events, allowing clients to register handlers prior to the initial session
    // enumeration
    public void Activate()
    {
        var sessionNotification = new AudioSessionNotification();
        sessionNotification.SessionCreated += SessionCreated;
        _sessionManager.RegisterSessionNotification(sessionNotification);

        EnumerateSessions();
    }

    private void EnumerateSessions()
    {
        using (var sessionEnumerator = _sessionManager.GetSessionEnumerator())
        {
            foreach (var session in sessionEnumerator)
            {
                AddSession(session);
                session.Dispose();
            }
        }
    }

    public bool SetVolume(SessionId id, float volume)
    {
        AudioSessionControl2 session;
        bool found = _sessions.TryGetValue(id, out session);

        // The session might have been removed between the client changing the volume and that
        // message arriving here. In this case we're just not going to do anything, and we'll
        // return false to the user. We're assuming here that the session ID cannot be unassigned
        // and reassigned in the time it takes the end user's message to arrive. IdStore's
        // implementation supports this assumption by assigning all IDs incrementally before
        // wrapping around.
        if (found)
        {
            session.QueryInterface<SimpleAudioVolume>().MasterVolume = volume;
        }

        return found;
    }

    public bool SetMute(SessionId id, bool mute)
    {
        AudioSessionControl2 session;
        bool found = _sessions.TryGetValue(id, out session);

        // See comment in SetVolume
        if (found)
        {
            session.QueryInterface<SimpleAudioVolume>().IsMuted = mute;
        }

        return found;
    }

    // Does not take ownership of the passed session
    private void AddSession(AudioSessionControl session)
    {
        var session2 = session.QueryInterface<AudioSessionControl2>();
        AddSession(session.QueryInterface<AudioSessionControl2>());
    }

    // Takes ownership of the passed session
    private void AddSession(AudioSessionControl2 session)
    {
        var process = session.Process;

        if (process == null)
        {
            Debug.WriteLine("Ignoring session not attached to a process");
            session.Dispose();
            return;
        }

        // TODO what if the process exits before we register the exit handler?

        var sessionId = session.SessionInstanceIdentifier;

        var added = _sessions.TryAdd(sessionId, session);
        Debug.Assert(added);

        // Register a handler that removes this session when its associated process terminates.
        // Note that there are unknowns here around multi-process sessions. Multi-process sessions
        // return the spawning process when asked, so when that process exits we'll let go of the
        // audio session. That might result in removing the session early
        try
        {
            process.EnableRaisingEvents = true;
            process.Exited += ConvertSessionProcessExitedHandler(sessionId, SessionProcessExited);
        }
        catch (Win32Exception)
        {
            // Just assume that access-restricted processes will stick around
        }

        AudioSessionEvents sessionEvents = new AudioSessionEvents();
        sessionEvents.SimpleVolumeChanged +=
            ConvertSessionEventHandler<AudioSessionSimpleVolumeChangedEventArgs>(sessionId, InternalVolumeChanged);
        sessionEvents.StateChanged +=
            ConvertSessionEventHandler<AudioSessionStateChangedEventArgs>(sessionId, InternalStateChanged);
        sessionEvents.SessionDisconnected +=
            ConvertSessionEventHandler<AudioSessionDisconnectedEventArgs>(sessionId, InternalDisconnected);

        session.RegisterAudioSessionNotification(sessionEvents);

        OnSessionAdded(new SessionAddedEventArgs(sessionId, session));
    }

    // The passed session ID must be that of a session owned by the class. This call will dispose
    // the session
    private void RemoveSession(SessionId sessionId)
    {
        AudioSessionControl2 removedSession;
        bool res = _sessions.TryRemove(sessionId, out removedSession);
        Debug.Assert(res, "session to remove must be in stored session list");

        removedSession.Dispose();

        OnSessionRemoved(new SessionRemovedEventArgs(sessionId));
    }

    // Invokes the SessionAdded event
    protected virtual void OnSessionAdded(SessionAddedEventArgs e)
    {
        SessionAdded?.Invoke(this, e);
    }

    // Invokes the SessionRemoved event
    protected virtual void OnSessionRemoved(SessionRemovedEventArgs e)
    {
        SessionRemoved?.Invoke(this, e);
    }

    // Invokes the VolumeChanged event
    protected virtual void OnVolumeChanged(VolumeChangedEventArgs e)
    {
        VolumeChanged?.Invoke(this, e);
    }

    // Invokes the StateChanged event
    protected virtual void OnStateChanged(StateChangedEventArgs e)
    {
        StateChanged?.Invoke(this, e);
    }

    protected virtual void OnDisconnected(DisconnectedEventArgs e)
    {
        Disconnected?.Invoke(this, e);
    }

    #region Internal event handling
    // Handler for the SessionCreated event on an AudioSessionNotification
    private void SessionCreated(object sender, SessionCreatedEventArgs e)
    {
        AddSession(e.NewSession);
        // For unknown reasons, disposing the passed session causes session notifications to
        // be duplicated, and later on a bunch of access violations occur
        //e.NewSession.Dispose();
    }

    private void SessionProcessExited(object sender, EventArgs e,
        SessionId sessionId)
    {
        RemoveSession(sessionId);
    }

    private void InternalVolumeChanged(object sender, AudioSessionSimpleVolumeChangedEventArgs e,
        SessionId sessionId)
    {
        OnVolumeChanged(new VolumeChangedEventArgs(sessionId, e.NewVolume, e.IsMuted));
    }

    private void InternalStateChanged(object sender, AudioSessionStateChangedEventArgs e,
        SessionId sessionId)
    {
        OnStateChanged(new StateChangedEventArgs(sessionId, e.NewState));
    }

    private void InternalDisconnected(object sender, AudioSessionDisconnectedEventArgs e,
        SessionId sessionId)
    {
        OnDisconnected(new DisconnectedEventArgs(sessionId, e.DisconnectReason));
    }

    private delegate void SessionEventHandler<T>(object sender, T e, SessionId sessionId);

    private EventHandler<T> ConvertSessionEventHandler<T>(SessionId sessionId,
        SessionEventHandler<T> handler)
    {
        return (object sender, T e) =>
        {
            handler(sender, e, sessionId);
        };
    }

    private delegate void SessionProcessExitedHandler(object sender, EventArgs e,
        SessionId sessionId);

    private EventHandler ConvertSessionProcessExitedHandler(SessionId sessionId,
        SessionProcessExitedHandler handler)
    {
        return (object sender, EventArgs e) =>
        {
            handler(sender, e, sessionId);
        };
    }
    #endregion

    private ConcurrentDictionary<SessionId, AudioSessionControl2> _sessions;

    private readonly MMDevice _device;
    private readonly AudioSessionManager2 _sessionManager;

    #region IDisposable Support
    private bool _disposed = false; // To detect redundant calls

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _sessionManager.Dispose();

                foreach (var pair in _sessions)
                {
                    pair.Value.Dispose();
                }

                _sessions.Clear();

                _device.Dispose();
            }

            _disposed = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
    }
    #endregion
}