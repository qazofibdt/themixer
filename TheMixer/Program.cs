﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Linq;

using SessionId = System.String;
using ProcessId = System.UInt32;

class Program
{
    static void Main(string[] args)
    {
        using (var sessions = new AudioSessions())
        {
            sessions.SessionAdded += SessionAdded;
            sessions.SessionRemoved += SessionRemoved;
            sessions.VolumeChanged += VolumeChanged;
            sessions.StateChanged += StateChanged;
            sessions.Disconnected += Disconnected;

            Console.WriteLine("Running... [Enter] to quit");

            sessions.Activate();

            while (!(Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Enter))
            {
                sessions.PrintSessions();

                var foregroundProcessId = GetForegroundProcessId();

                SessionId foregroundAudioSessionId;
                bool found = _sessionIdLookup.TryGetValue(foregroundProcessId, out foregroundAudioSessionId);

                string foregroundAudioSessionName = "(None)";

                if (found)
                {
                    found = _sessionNameLookup.TryGetValue(foregroundAudioSessionId, out foregroundAudioSessionName);

                    if (!found)
                        foregroundAudioSessionName = "(Removed)";
                }

                Console.WriteLine("Active session name: " + foregroundAudioSessionName);

                Thread.Sleep(3000);
            }

            Console.WriteLine("Done");
        }
    }

    private static ProcessId GetForegroundProcessId()
    {
        IntPtr hwnd = User32.GetForegroundWindow();
        ProcessId pid;
        User32.GetWindowThreadProcessId(hwnd, out pid);
        return pid;
    }

    private static void SessionAdded(object sender, SessionAddedEventArgs e)
    {
        string sessionName = e.Session.Process.ProcessName;
        if (e.Session.IsSystemSoundSession)
            sessionName += " (system)";
        if (!e.Session.IsSingleProcessSession)
            sessionName += " (multi-process)";

        _sessionNameLookup.TryAdd(e.SessionId, sessionName);
        _sessionIdLookup.TryAdd((ProcessId)e.Session.ProcessID, e.SessionId);

        Console.WriteLine("Found session: " + sessionName);
    }

    private static void SessionRemoved(object sender, SessionRemovedEventArgs e)
    {
        string sessionName;
        var res = _sessionNameLookup.TryRemove(e.SessionId, out sessionName);
        Debug.Assert(res);

        foreach (var item in _sessionIdLookup.Where(kvp => kvp.Value == e.SessionId).ToList())
        {
            SessionId id;
            res = _sessionIdLookup.TryRemove(item.Key, out id);
            Debug.Assert(res);
        }

        Console.WriteLine("Session released: " + sessionName);
    }

    private static void VolumeChanged(object sender, VolumeChangedEventArgs e)
    {
        Console.WriteLine("Session {0} ({1}) volume changed to {2}{3}",
            e.SessionId, _sessionNameLookup[e.SessionId], e.Volume, e.IsMuted ? " (muted)" : "");
    }

    private static void StateChanged(object sender, StateChangedEventArgs e)
    {
        Console.WriteLine("Session {0} ({1}) state changed to {2}",
            e.SessionId, _sessionNameLookup[e.SessionId], e.State);
    }

    private static void Disconnected(object sender, DisconnectedEventArgs e)
    {
        Console.WriteLine("Session {0} ({1}) disconnected for reason: {2}",
            e.SessionId, _sessionNameLookup[e.SessionId], e.Reason);
    }

    private static ConcurrentDictionary<SessionId, string> _sessionNameLookup =
        new ConcurrentDictionary<SessionId, string>();
    private static ConcurrentDictionary<ProcessId, SessionId> _sessionIdLookup =
        new ConcurrentDictionary<ProcessId, SessionId>();
}